
import state from './state'
import action from './actions'
import getters from './getters'
import mutations from './mutations'

const myCustomModule = {
    namespace: true,
    actions,
    getters,
    mutations,
    state
}


export  default myCustomModule