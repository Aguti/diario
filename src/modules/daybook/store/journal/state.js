

export  default () => ({
    isLoading: true,
    entries: [
        {
            id: new Date().getTime(),
            date: new Date().toDateString(),
            text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequuntur aperiam eveniet corrupti laudantium ipsa dicta perspiciatis aliquam doloribus deserunt repudiandae? Temporibus officiis illo itaque hic quasi ex ad architecto repellendus!\n',
            picture: null,
        },

        {
            id: new Date().getTime() + 1000,
            date: new Date().toDateString(),
            text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequuntur aperiam eveniet corrupti laudantium ipsa dicta perspiciatis aliquam doloribus deserunt repudiandae? Temporibus officiis illo itaque hic quasi ex ad architecto repellendus!\n',
            picture: null,
        },

        {
            id: new Date().getTime() + 2000,
            date: new Date().toDateString(),
            text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequuntur aperiam eveniet corrupti laudantium ipsa dicta perspiciatis aliquam doloribus deserunt repudiandae? Temporibus officiis illo itaque hic quasi ex ad architecto repellendus!\n',
            picture: null,
        },
    ]
})
