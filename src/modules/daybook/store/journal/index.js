
import state from './state'
import action from './actions'
import getters from './getters'
import mutations from './mutations'

const journalModule = {
    namespace: true,
    action,
    getters,
    mutations,
    state
}


export  default journalModule